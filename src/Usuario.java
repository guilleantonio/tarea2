/**
 * Created by Guillo on 8/07/2017.
 */
public class Usuario {
    private String codUsuario;
    private String nombre;
    private String dpi;

    public Usuario() {
        this.codUsuario = codUsuario;
        this.nombre = nombre;
        this.dpi = dpi;
    }


    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario() {
        this.codUsuario = codUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDpi() {
        return dpi;
    }

    public void setDpi(String dpi) {
        this.dpi = dpi;
    }
}
