import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guillo on 8/07/2017.
 */
public class Venta {
    private Producto[] listado;
    private List<Cliente> listadoCliente;
    private Usuario user;
    private int cantReal;

    public Venta(int cantidadProducto) {
        listado = new Producto[cantidadProducto];
        cantReal = 0;
        listadoCliente = new ArrayList<>();


    }

    public Producto[] getListado() {
        return listado;
    }

    public void setListado(Producto[] listado) {
        this.listado = listado;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public int getCantReal() {
        return cantReal;
    }

    public void setCantReal(int cantReal) {
        this.cantReal = cantReal;
    }

    public List<Cliente> getListadoCliente() {
        return listadoCliente;
    }

    public void setListadoCliente(List<Cliente> listadoCliente) {
        this.listadoCliente = listadoCliente;
    }

    public void adiciocionarCliente(Cliente c){
        listadoCliente.add(c);

    }

    public void adicionarProducto(Producto p) throws Exception {

        if (cantReal < listado.length) {
                listado[cantReal] = p;
            cantReal++;

        } else
            throw new Exception("Imposible adicionar más Productos");


    }

}
